//!
//! Generative tests for _additive_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn left_inverse_i32(x in any::<i32>()) {
    prop_assert!(left_inverse(&x))
  }


  #[test]
  fn left_inverse_i16_t1(x in any::<i16>()) {
    prop_assert!(left_inverse(&(x,)))
  }


  #[test]
  fn left_inverse_i8_a1(x in any::<i8>()) {
    prop_assert!(left_inverse(&[x]))
  }


  #[test]
  fn right_inverse_i16(x in any::<i16>()) {
    prop_assert!(right_inverse(&x))
  }


  #[test]
  fn right_inverse_isize_t2(xs in any::<(isize, isize)>()) {
    prop_assert!(right_inverse(&xs))
  }


  #[test]
  fn right_inverse_i64_a2(xs in any::<[i64; 2]>()) {
    prop_assert!(right_inverse(&xs))
  }


  #[test]
  fn left_inverse_f64(x in TF64) {
    prop_assert!(num_left_inverse(&x, &F64_EPS))
  }


  #[test]
  fn right_inverse_f32(x in TF32) {
    prop_assert!(num_right_inverse(&x, &F32_EPS))
  }
}
