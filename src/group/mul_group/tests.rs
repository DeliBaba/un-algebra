//!
//! Generative tests for _multiplicative_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {

  // Allow more value rejects to find invertible tuples.
  #![proptest_config(config_with(300, 4000))]


  #[test]
  fn left_inverse_f32(x in TF32) {
    prop_assume!(x.is_invertible());

    prop_assert!(num_left_inverse(&x, &F32_EPS))
  }


  #[test]
  fn left_inverse_f64_t1(x in TF64) {
    prop_assume!((x,).is_invertible());

    prop_assert!(num_left_inverse(&(x,), &F64_EPS))
  }


  #[test]
  fn left_inverse_f32_a1(x in TF32) {
    prop_assume!([x].is_invertible());

    prop_assert!(num_left_inverse(&[x], &F32_EPS))
  }


  #[test]
  fn right_inverse_f64_t1(x in TF64) {
    prop_assume!((x,).is_invertible());

    prop_assert!(num_right_inverse(&(x,), &F64_EPS))
  }
}
