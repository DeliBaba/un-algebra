//!
//! Algebraic _group_ modules.
//!
//! The `group` module provides sub-modules for abstract algebraic
//! _groups_. It also provides sub-modules for _additive_ or
//! _multiplicative groups_.
//!
pub mod group;
pub mod add_group;
pub mod mul_group;


// Make sub-modules visible in this module.
pub use self::group::*;
pub use self::add_group::*;
pub use self::mul_group::*;
