//!
//! Algebraic _additive_ _groups_.
//!
//! An algebraic _additive_ _group_ is an _additive_ _monoid_ `M`, where
//! each group element `g` has a unique additive _inverse_ element
//! denoted `-g`. The inverse operation is called _negation_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀g, 0 ∈ M
//!
//! Inverse: ∃-g ∈ M: g + -g = -g + g = 0.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive group.
//!
#![doc(include = "../doc/references.md")]

use crate::numeric::*;
use crate::monoid::*;
use crate::helpers::*;


///
/// An algebraic _additive group_.
///
pub trait AddGroup: AddMonoid {

  /// The unique _additive_ _inverse_ of a group element.
  fn negate(&self) -> Self;


  /// The additive "subtraction" of two group elements.
  fn sub(&self, other: &Self) -> Self {
    self.add(&other.negate())
  }
}


///
/// The _left_ _additive_ _inverse_ axiom.
///
pub fn left_inverse<T: AddGroup>(x: &T) -> bool {
  x.negate().add(x) == T::zero()
}


///
/// The _right_ _additive_ _inverse_ axiom.
///
pub fn right_inverse<T: AddGroup>(x: &T) -> bool {
  x.add(&x.negate()) == T::zero()
}


///
/// The _two_-_sided_ _additive_ inverse axiom.
///
pub fn inverse<T: AddGroup>(x: &T) -> bool {
  left_inverse(x) && right_inverse(x)
}


///
/// The _left_ _numerical_ _additive_ _inverse_ axiom.
///
pub fn num_left_inverse<T: AddGroup + NumEq>(x: &T, eps: &T::Eps) -> bool {
  x.negate().add(x).num_eq(&T::zero(), eps)
}


///
/// The _right_ _numerical_ _additive_ _inverse_ axiom.
///
pub fn num_right_inverse<T: AddGroup + NumEq>(x: &T, eps: &T::Eps) -> bool {
  x.add(&x.negate()).num_eq(&T::zero(), eps)
}


///
/// The _two_ _sided_ _numerical_ _additive_ _inverse_ axiom.
///
pub fn num_inverse<T: AddGroup + NumEq>(x: &T, eps: &T::Eps) -> bool {
  num_left_inverse(x, eps) && num_right_inverse(x, eps)
}


///
/// A macro for `AddGroup` implementations for built-in signed integer
/// types. Probably not needed if Rust had a signed `Integer`
/// super-trait.
///
macro_rules! integer_add_group {
  ($type:ty) => {
    impl AddGroup for $type {

      /// Additive group negation uses "wrapping" negate to avoid
      /// overflow and guarantee the closure axiom.
      fn negate(&self) -> Self {
        self.wrapping_neg()
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    integer_add_group! {$type}
    integer_add_group! {$($others),+}
  };
}


// Only signed integers form an additive group.
integer_add_group! {
  i8, i16, i32, i64, i128, isize
}


///
/// A macro for `AddGroup` implementations for built-in floating point
/// types. Probably not needed if Rust had a `Float` super-trait.
///
macro_rules! float_add_group {
  ($type:ty) => {
    impl AddGroup for $type {

      // Negation is just floating point negation.
      fn negate(&self) -> Self {
        -*self
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    float_add_group! {$type}
    float_add_group! {$($others),+}
  };
}


// Additive group floating point types.
float_add_group! {
  f32, f64
}


///
/// 0-tuples form an additive group.
///
impl AddGroup for () {

  /// Negated value can only be `()`.
  fn negate(&self) -> Self {}
}


///
/// 1-tuples form an additive group when their items do.
///
impl<A: AddGroup> AddGroup for (A,) {

  /// Negation is by element type.
  fn negate(&self) -> Self {
    (self.0.negate(), )
  }
}


///
/// 2-tuples form an additive group when their items do.
///
impl<A: AddGroup, B: AddGroup> AddGroup for (A, B) {

  /// Negation is by element type.
  fn negate(&self) -> Self {
    (self.0.negate(), self.1.negate())
  }
}


///
/// 3-tuples form an additive group when their items do.
///
impl<A: AddGroup, B: AddGroup, C: AddGroup> AddGroup for (A, B, C) {

  /// Negation is by element type.
  fn negate(&self) -> Self {
    (self.0.negate(), self.1.negate(), self.2.negate())
  }
}


///
/// A macro for `AddGroup` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_add_group {
  ($size:expr) => {
    impl<T: Copy + AddGroup> AddGroup for [T; $size] {

     // Negate all array items.
     fn negate(&self) -> Self {
       self.map(&|&x| x.negate())
     }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_add_group! {$size}
    array_add_group! {$($others),+}
  };
}


// Array additive group types.
array_add_group! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
