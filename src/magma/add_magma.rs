//!
//! Algebraic _additive_ _magmas_.
//!
//! An algebraic _additive_ _magma_ is a set `S`, equipped with a
//! _binary_ _operation_ `+`, called _addition_. `S` is _closed_ under
//! addition.
//!
//! # Axioms
//!
//! ```notrust
//! Closure: ∀x, y ∈ S, x + y ∈ S.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive magma.
//!
#![doc(include = "../doc/references.md")]

use crate::numeric::*;
use crate::helpers::*;


///
/// An algebraic _additive magma_.
///
pub trait AddMagma: Sized + PartialEq {

  /// Binary _addition_ operation.
  fn add(&self, other: &Self) -> Self;
}


///
/// The _closure_ axiom. Guaranteed by Rust's type system and
/// implemented only for completeness.
///
pub fn closure<T: AddMagma>(_x: &T, _y: &T) -> bool {
  true
}


///
/// The numeric _closure_ axiom. Guaranteed by Rust's type system and
/// implemented only for completeness.
///
pub fn num_closure<T: AddMagma + NumEq>(_x: &T, _y: &T, _eps: &T::Eps) -> bool {
  true
}


///
/// A macro for `AddMagma` implementations for built-in integer types.
/// Probably not needed if Rust had an `Integer` super-trait.
///
macro_rules! integer_add_magma {
  ($type:ty) => {

    impl AddMagma for $type {

      /// We use "wrapping" add to avoid overflow and guarantee the
      /// closure axiom.
      fn add(&self, other: &Self) -> Self {
        self.wrapping_add(*other)
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    integer_add_magma! {$type}
    integer_add_magma! {$($others),+}
  };
}


// AddMagma implementation for integer types.
integer_add_magma! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize
}


///
/// A macro for `AddMagma` implementations for built-in floating point
/// types. Probably not needed if Rust had a `Float` super-trait.
///
macro_rules! float_add_magma {
  ($type:ty) => {

    impl AddMagma for $type {

      /// magma addition is just floating point addition.
      fn add(&self, other: &Self) -> Self {
        *self + *other
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    float_add_magma! {$type}
    float_add_magma! {$($others),+}
  };
}


// Floating point additive magma types.
float_add_magma! {
  f32, f64
}


///
/// 0-tuples form an additive magma.
///
impl AddMagma for () {

  /// Addition can only yield a `()`.
  fn add(&self, _: &Self) -> Self {}
}


///
/// 1-tuples form an additive magma when their items do.
///
impl<A: AddMagma> AddMagma for (A,) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), )
  }
}


///
/// 2-tuples form an additive magma when their items do.
///
impl<A: AddMagma, B: AddMagma> AddMagma for (A, B) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), self.1.add(&other.1))
  }
}


///
/// 3-tuples form an additive magma when their items do.
///
impl<A: AddMagma, B: AddMagma, C: AddMagma> AddMagma for (A, B, C) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), self.1.add(&other.1), self.2.add(&other.2))
  }
}


///
/// A macro for `AddMagma` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_add_magma {
  ($size:expr) => {
    impl<T: Copy + AddMagma> AddMagma for [T; $size] {

      // Delegate to `Sequence` trait map_with function.
      fn add(&self, other: &Self) -> Self {
        self.map_with(other, &|x, y| x.add(y))
      }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_add_magma! {$size}
    array_add_magma! {$($others),+}
  };
}


// Array additive magma types.
array_add_magma! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;

