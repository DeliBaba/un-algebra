//!
//! Generative tests for _quasigroups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {

  // Allow more value rejects to find non-zero tuples.
  #![proptest_config(config_with(10_000, 9_000))]


  #[test]
  fn left_lcancellation_f32([x, y] in [TF32, TF32]) {
    prop_assume!(x.mul(y).is_divisor());

    prop_assert!(num_left_lcancellation(&x, &y, &F32_EPS))
  }


  #[test]
  fn right_lcancellation_f32([x, y] in [TF32, TF32]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_right_lcancellation(&x, &y, &F32_EPS))
  }


  #[test]
  fn left_rcancellation_f32([x, y] in [TF32, TF32]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_left_rcancellation(&x, &y, &F32_EPS))
  }


  #[test]
  fn right_rcancellation_f32([x, y] in [TF32, TF32]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_right_rcancellation(&x, &y, &F32_EPS))
  }


  #[test]
  fn left_lcancellation_f64([x, y] in [TF64, TF64]) {
    prop_assume!(x.mul(y).is_divisor());

    prop_assert!(num_left_lcancellation(&x, &y, &F64_EPS))
  }


  #[test]
  fn right_lcancellation_f64([x, y] in [TF64, TF64]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_right_lcancellation(&x, &y, &F64_EPS))
  }


  #[test]
  fn left_rcancellation_f64([x, y] in [TF64, TF64]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_left_rcancellation(&x, &y, &F64_EPS))
  }


  #[test]
  fn right_rcancellation_f64([x, y] in [TF64, TF64]) {
    prop_assume!(y.is_divisor());

    prop_assert!(num_right_rcancellation(&x, &y, &F64_EPS))
  }
}
