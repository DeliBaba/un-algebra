//!
//! Helper types and functions for logic and sets.
//!

/// Test logical implication `x` => `y`.
pub fn imply(x: bool, y: bool) -> bool {
  (!x) || y
}


/// Test logical equivalence `x` <=> `y`.
pub fn iff(x: bool, y: bool) -> bool {
  imply(x, y) && imply(y, x)
}
