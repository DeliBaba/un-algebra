//!
//! Algebraic _inequality_ _relations_.
//!
//! An _inequality_ _relation_ on a set `S` is a _binary_ _relation_ `R`
//! on `S` (written `xRx` for ∀x ∈ `S`), with _irreflexive_ and
//! _symmetric_ properties.
//!
//! # Properties
//!
//! ```notrust
//! ∀x, y ∈ S
//!
//! Irreflexive: not xRx.
//! Symmetric: xRy ⇒ yRx.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an inequality relation.
//!
#![doc(include = "../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _inequality_ _relation_.
///
pub trait Inequality: Sized {}


///
/// The property of (inequality) _symmetry_.
///
pub fn symmetry<T: Inequality>(f: &Rel<T>, x: &T, y: &T) -> bool {
  imply(f(x, y), f(y, x))
}


///
/// The property of (equivalence) _irreflexivity_.
///
pub fn irreflexivity<T: Inequality>(f: &Rel<T>, x: &T) -> bool {
  !f(x, x)
}


///
/// The property of numeric (inequality) _symmetry_.
///
pub fn num_symmetry<T: Inequality + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps), f(y, x, eps))
}


///
/// The property of numeric (inequality) _irreflexivity_.
///
pub fn num_irreflexivity<T: Inequality + NumEq>(f: &NumRel<T>, x: &T, eps: &T::Eps) -> bool {
  !f(x, x, eps)
}


