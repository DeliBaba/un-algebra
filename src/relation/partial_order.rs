//!
//! Algebraic (non-strict) _partial_ _orders_.
//!
//! A (non-strict) _partial_ _order_ on a set `S` is a _binary_
//! _relation_ `R` on `S` (written `xRx` for ∀x ∈ `S`), with
//! _anti-symmetric_, _reflexive_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```notrust
//! ∀x, y, z ∈ S
//!
//! Reflexive: xRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! Anti-symmetric: xRy Λ yRx ⇒ x = y.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a (non-strict) partial
//! order.
//!
#![doc(include = "../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic (non-strict) _partial_ _order_ _relation_.
///
pub trait PartialOrder: PartialEq {}


///
/// The property of (partial order) _anti_ _symmetry_.
///
pub fn antisymmetry<T: PartialOrder>(f: &Rel<T>, x: &T, y: &T) -> bool {
  imply(f(x, y) && f(y, x), x == y)
}


///
/// The property of (partial order) _reflexivity_.
///
pub fn reflexivity<T: PartialOrder>(f: &Rel<T>, x: &T) -> bool {
  f(x, x)
}


///
/// The property of (partial order) _transitivity_.
///
pub fn transitivity<T: PartialOrder>(f: &Rel<T>, x: &T, y: &T, z: &T) -> bool {
  imply(f(x, y) && f(y, z), f(x, z))
}


///
/// The property of numeric (partial order) _anti_ _symmetry_.
///
pub fn num_antisymmetry<T: PartialOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, x, eps), x.num_eq(y, eps))
}


///
/// The property of numeric (partial order) _reflexivity_.
///
pub fn num_reflexivity<T: PartialOrder + NumEq>(f: &NumRel<T>, x: &T, eps: &T::Eps) -> bool {
  f(x, x, eps)
}


///
/// The property of numeric (partial order) _transitivity_.
///
pub fn num_transitivity<T: PartialOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
}


