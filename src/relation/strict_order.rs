//!
//! Algebraic _strict_ _partial_ _orders_.
//!
//! A _strict_ _partial_ _order_ (or _strict_ _order_) on a set `S` is a
//! _binary_ _relation_ `R` on `S` (written `xRx` for ∀x ∈ `S`), with
//! _asymmetric_, _irreflexive_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```notrust
//! ∀x, y, z ∈ S
//!
//! Irreflexive: ¬xRx.
//! Asymmetric: xRy ⇒ ¬yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a strict partial
//! order.
//!
#![doc(include = "../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _strict_ _partial_ _order_ _relation_.
///
pub trait StrictOrder: Sized {}


///
/// The property of (strict order) _irreflexivity_.
///
pub fn irreflexivity<T: StrictOrder>(f: &Rel<T>, x: &T) -> bool {
  !f(x, x)
}


///
/// The property of (strict order) _asymmetry_.
///
pub fn asymmetry<T: StrictOrder>(f: &Rel<T>, x: &T, y: &T) -> bool {
  imply(f(x, y), !f(y, x))
}


///
/// The property of (strict order) _transitivity_.
///
pub fn transitivity<T: StrictOrder>(f: &Rel<T>, x: &T, y: &T, z: &T) -> bool {
  imply(f(x, y) && f(y, z), f(x, z))
}


///
/// The property of numeric (strict order) _irreflexivity_.
///
pub fn num_irreflexivity<T: StrictOrder + NumEq>(f: &NumRel<T>, x: &T, eps: &T::Eps) -> bool {
  !f(x, x, eps)
}


///
/// The property of numeric (strict order) _asymmetry_.
///
pub fn num_asymmetry<T: StrictOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps), !f(y, x, eps))
}


///
/// The property of numeric (strict order) _transitivity_.
///
pub fn num_transitivity<T: StrictOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
}


