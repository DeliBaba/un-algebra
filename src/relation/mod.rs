//!
//! Algebraic _relation_ modules.
//!
//! The `relation` module provides sub-modules for algebraic _n-ary_
//! _relations_.
//!
pub mod relation;
pub mod inequality;
pub mod equivalence;
pub mod total_order;
pub mod strict_order;
pub mod partial_order;


// Make sub-modules visible in this module.
pub use self::relation::*;
pub use self::inequality::*;
pub use self::equivalence::*;
pub use self::total_order::*;
pub use self::strict_order::*;
pub use self::partial_order::*;
