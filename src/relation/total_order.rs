//!
//! Algebraic _total_ _orders_.
//!
//! A _total_ _order_ on a set `S` is a _binary_ _relation_ `R` on `S`
//! (written `xRx` for ∀x ∈ `S`), with _anti-symmetric_, _connex_ and
//! _transitive_ properties.
//!
//! # Properties
//!
//! ```notrust
//! ∀x, y, z ∈ S
//!
//! Connex: xRy ∨ yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! Anti-symmetric: xRy Λ yRx ⇒ x = y.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a total order.
//!
#![doc(include = "../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _total_ _order_ _relation_.
///
pub trait TotalOrder: PartialEq {}


///
/// The property of (total order) _connexity_.
///
pub fn connexity<T: TotalOrder>(f: &Rel<T>, x: &T, y: &T) -> bool {
  f(x, y) || f(y, x)
}


///
/// The property of (total order) _anti_ _symmetry_.
///
pub fn antisymmetry<T: TotalOrder>(f: &Rel<T>, x: &T, y: &T) -> bool {
  imply(f(x, y) && f(y, x), x == y)
}


///
/// The property of (total order) _transitivity_.
///
pub fn transitivity<T: TotalOrder>(f: &Rel<T>, x: &T, y: &T, z: &T) -> bool {
  imply(f(x, y) && f(y, z), f(x, z))
}


///
/// The property of numeric (total order) _connexity_.
///
pub fn num_connexity<T: TotalOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  f(x, y, eps) || f(y, x, eps)
}


///
/// The property of numeric (total order) _anti_ _symmetry_.
///
pub fn num_antisymmetry<T: TotalOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, x, eps), x.num_eq(y, eps))
}


///
/// The property of numeric (total order) _transitivity_.
///
pub fn num_transitivity<T: TotalOrder + NumEq>(f: &NumRel<T>, x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
}


