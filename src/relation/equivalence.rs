//!
//! Algebraic _equivalence_ _relations_.
//!
//! An _equivalence_ _relation_ on a set `S` is a _binary_ _relation_
//! `R` on `S` (written `xRx` for ∀x ∈ `S`), with _reflexive_,
//! _symmetric_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```notrust
//! ∀x, y, z ∈ S
//!
//! Reflexive: xRx.
//! Symmetric: xRy ⇒ yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an equivalence relation.
//!
#![doc(include = "../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _equivalence_ _relation_.
///
pub trait Equivalence: Sized {}


///
/// The property of (equivalence) _symmetry_.
///
pub fn symmetry<T: Equivalence>(f: &Rel<T>, x: &T, y: &T) -> bool {
  imply(f(x, y), f(y, x))
}


///
/// The property of (equivalence) _reflexivity_.
///
pub fn reflexivity<T: Equivalence>(f: &Rel<T>, x: &T) -> bool {
  f(x, x)
}


///
/// The property of (equivalence) _transitivity_.
///
pub fn transitivity<T: Equivalence>(f: &Rel<T>, x: &T, y: &T, z: &T) -> bool {
  imply(f(x, y) && f(y, z), f(x, z))
}


///
/// The property of numeric (equivalence) _symmetry_.
///
pub fn num_symmetry<T: Equivalence + NumEq>(f: &NumRel<T>, x: &T, y: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps), f(y, x, eps))
}


///
/// The property of numeric (equivalence) _reflexivity_.
///
pub fn num_reflexivity<T: Equivalence + NumEq>(f: &NumRel<T>, x: &T, eps: &T::Eps) -> bool {
  f(x, x, eps)
}


///
/// The property of numeric (equivalence) _transitivity_.
///
pub fn num_transitivity<T: Equivalence + NumEq>(f: &NumRel<T>, x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
}

