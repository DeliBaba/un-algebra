//!
//! Algebraic _field_ modules.
//!
//! The `field` module provides sub-modules for abstract algebraic
//! _fields_.
//!
pub mod field;


// Make sub-modules visible in this module.
pub use self::field::*;

