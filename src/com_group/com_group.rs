//!
//! Algebraic _commutative groups_.
//!
//! An algebraic _commutative group_ (also called an _abelian_ _group_)
//! is a _group_ `M`, where the group operation `∘` is required to be
//! _commutative_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀g, h ∈ M
//!
//! Commutativity: g ∘ h = h ∘ g.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a commutative group.
//!
#![doc(include = "../doc/references.md")]

use crate::group::*;


///
/// An algebraic _commutative group_.
///
pub trait ComGroup: Group {}


///
/// The _commutivity_ axiom.
///
pub fn commutivity<T: ComGroup>(x: &T, y: &T) -> bool {
  x.op(y) == y.op(x)
}


///
/// 0-tuples form a commutative group.
///
impl ComGroup for () {}


///
/// 1-tuples form a commutative group when their items do.
///
impl<A: ComGroup> ComGroup for (A,) {}


///
/// 2-tuples form a commutative group when their items do.
///
impl<A: ComGroup, B: ComGroup> ComGroup for (A, B) {}


///
/// 3-tuples form a commutative group when their items do.
///
impl<A: ComGroup, B: ComGroup, C: ComGroup> ComGroup for (A, B, C) {}


///
/// A macro for `ComGroup` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_com_group {
  ($size:expr) => {
    impl<T: Copy + ComGroup> ComGroup for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_com_group! {$size}
    array_com_group! {$($others),+}
  };
}


// Array group types.
array_com_group! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}
