//!
//! Algebraic _additive_ _commutative_ _groups_.
//!
//! An algebraic _additive_ _commutative_ _group_ is an _additive_
//! _group_ `G`, where group addition `+` is required to be
//! _commutative_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀g, h ∈ G
//!
//! Commutativity: g + h = h + g.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive commutative
//! group.
//!
#![doc(include = "../doc/references.md")]

use crate::group::*;
use crate::numeric::*;


///
/// An algebraic _additive commutative group_.
///
pub trait AddComGroup: AddGroup {}


///
/// The _additive_ _commutivity_ axiom.
///
pub fn commutivity<T: AddComGroup>(x: &T, y: &T) -> bool {
  x.add(y) == y.add(x)
}


///
/// The _numeric_ _additive_ _commutivity_ axiom.
///
pub fn num_commutivity<T: AddComGroup + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  x.add(y).num_eq(&y.add(x), eps)
}


///
/// A macro for `AddComGroup` implementations for built-in numeric
/// types.
///
macro_rules! numeric_add_com_group {
  ($type:ty) => {
    impl AddComGroup for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_add_com_group! {$type}
    numeric_add_com_group! {$($others),+}
  };
}


// Signed integers and floats form additive commutative groups.
numeric_add_com_group! {
  i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form an additive commutative group.
///
impl AddComGroup for () {}


///
/// 1-tuples form an additive commutative group when their items do.
///
impl<A: AddComGroup> AddComGroup for (A,) {}


///
/// 2-tuples form an additive commutative group when their items do.
///
impl<A: AddComGroup, B: AddComGroup> AddComGroup for (A, B) {}


///
/// 3-tuples form an additive commutative group when their items do.
///
impl<A: AddComGroup, B: AddComGroup, C: AddComGroup> AddComGroup for (A, B, C) {}


///
/// A macro for `AddComGroup` implementations for arrays. Maybe not
/// needed if Rust had _const_ _generics_.
///
macro_rules! array_add_com_group {
  ($size:expr) => {
    impl<T: Copy + AddComGroup> AddComGroup for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_add_com_group! {$size}
    array_add_com_group! {$($others),+}
  };
}


// Array commutative additive group types.
array_add_com_group! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;



