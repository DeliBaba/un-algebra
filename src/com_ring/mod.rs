//!
//! Algebraic _commutative_ _ring_ modules.
//!
//! The `com_ring` module provides sub-modules for abstract algebraic
//! _commutative_ _rings_.
//!
pub mod com_ring;


// Make sub-modules visible in the `com_ring` module.
pub use self::com_ring::*;

