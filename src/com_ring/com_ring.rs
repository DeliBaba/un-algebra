//!
//! Algebraic _commutative_ _rings_.
//!
//! An algebraic _commutative_ _ring_ `R`, is a _ring_ (formally a
//! _ring_ _with_ _identity_) where the _multiplication_ operation `×`
//! is _commutative_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀g, h ∈ R
//!
//! Commutivity: g × h = h × g.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a commutative ring.
//!
#![doc(include = "../doc/references.md")]

use crate::ring::*;
use crate::numeric::*;


///
/// An algebraic _commutative ring_.
///
pub trait ComRing: Ring {}


///
/// The axiom of _multiplicative_ _commutivity_.
///
pub fn commutivity<T: ComRing>(x: &T, y: &T) -> bool {
  x.mul(y) == y.mul(x)
}


///
/// The axiom of _numeric_ _multiplicative_ _commutivity_.
///
pub fn num_commutivity<T: ComRing + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  x.mul(y).num_eq(&y.mul(x), eps)
}


///
/// A macro for `ComRing` implementations for built-in numeric types.
///
macro_rules! numeric_com_ring {
  ($type:ty) => {
    impl ComRing for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_com_ring! {$type}
    numeric_com_ring! {$($others),+}
  };
}


// Only signed integer and float types form a commutative ring.
numeric_com_ring! {
  i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form a commutative ring.
///
impl ComRing for () {}


///
/// 1-tuples form a commutative ring when their items do.
///
impl<A: ComRing> ComRing for (A,) {}


///
/// 2-tuples form a commutative ring when their items do.
///
impl<A: ComRing, B: ComRing> ComRing for (A, B) {}


///
/// 3-tuples form a commutative ring when their items do.
///
impl<A: ComRing, B: ComRing, C: ComRing> ComRing for (A, B, C) {}


///
/// A macro for `ComRing` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_com_ring {
  ($size:expr) => {
    impl<T: Copy + ComRing> ComRing for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_com_ring! {$size}
    array_com_ring! {$($others),+}
  };
}


// Array commutative ring types.
array_com_ring! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
