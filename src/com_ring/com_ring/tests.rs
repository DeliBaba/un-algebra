//!
//! Generative tests for _commutative_ _rings_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn commutivity_i8((x, y) in any::<(i8, i8)>()) {
    prop_assert!(commutivity(&x, &y))
  }


  #[test]
  fn commutivity_i16_t2([xs, ys] in any::<[(i16, i16); 2]>()) {
    prop_assert!(commutivity(&xs, &ys))
  }


  #[test]
  fn commutivity_i16_a2([xs, ys] in any::<[[i16; 2]; 2]>()) {
    prop_assert!(commutivity(&xs, &ys))
  }


  #[test]
  fn commutivity_f32([x, y] in [TF32, TF32]) {
    prop_assert!(num_commutivity(&x, &y, &F32_EPS))
  }


  #[test]
  fn commutivity_f64([x, y] in [TF64, TF64]) {
    prop_assert!(num_commutivity(&x, &y, &F64_EPS))
  }
}
