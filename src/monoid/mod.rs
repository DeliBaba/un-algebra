//!
//! Algebraic _monoid_ modules.
//!
//! The `monoid` module provides sub-modules for abstract algebraic
//! _monoids_. It also provides sub-modules for _additive_ or
//! _multiplicative monoids_.
//!
pub mod monoid;
pub mod add_monoid;
pub mod mul_monoid;


// Make sub-modules visible in this module.
pub use self::monoid::*;
pub use self::add_monoid::*;
pub use self::mul_monoid::*;
