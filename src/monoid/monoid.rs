//!
//! Algebraic _monoids_.
//!
//! An algebraic _monoid_ is a _semigroup_ `S`, with a unique _identity_
//! element denoted `e`.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x ∈ S
//!
//! Identity: ∃e ∈ S: e ∘ x = x ∘ e = x.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a monoid.
//!
#![doc(include = "../doc/references.md")]

use crate::semigroup::*;


///
/// An algebraic _monoid_.
///
pub trait Monoid: Semigroup {

  /// The unique monoid _identity_ _element_.
  fn id() -> Self;


  /// Test for the identity value.
  fn is_id(&self) -> bool {
    *self == Self::id()
  }
}


///
/// The _left_ _identity_ axiom.
///
pub fn left_identity<T: Monoid>(x: &T) -> bool {
  T::id().op(x) == *x
}


///
/// The _right_ _identity_ axiom.
///
pub fn right_identity<T: Monoid>(x: &T) -> bool {
  x.op(&T::id()) == *x
}


///
/// The _two_ _sided_ _identity_ axiom.
///
pub fn identity<T: Monoid>(x: &T) -> bool {
  left_identity(x) && right_identity(x)
}


///
/// 0-tuples form a monoid.
///
impl Monoid for () {

  /// Identity value can only be `()`.
  fn id() -> Self {}
}


///
/// 1-tuples form a monoid when items do.
///
impl<T: Monoid> Monoid for (T,) {

  /// Identity is by element type.
  fn id() -> Self {
    (T::id(), )
  }
}


///
/// 2-tuples form a monoid when items do.
///
impl<A: Monoid, B: Monoid> Monoid for (A, B) {

  /// Identity is by element type.
  fn id() -> Self {
    (A::id(), B::id())
  }
}


///
/// 3-tuples form a monoid when items do.
///
impl<A: Monoid, B: Monoid, C: Monoid> Monoid for (A, B, C) {

  /// Identity is by element type.
  fn id() -> Self {
    (A::id(), B::id(), C::id())
  }
}


///
/// A macro for `Monoid` implementations for arrays. Maybe not needed if
/// Rust had _const_ _generics_.
///
macro_rules! array_monoid {
  ($size:expr) => {
    impl<T: Copy + Monoid> Monoid for [T; $size] {

      // Create array of identity values.
      fn id() -> Self {
        [T::id(); $size]
      }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_monoid! {$size}
    array_monoid! {$($others),+}
  };
}


// Array monoid types.
array_monoid! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}

