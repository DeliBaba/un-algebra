//!
//! Generative tests for _additive_ _monoids_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn left_identity_u8(x in any::<u8>()) {
    prop_assert!(left_identity(&x))
  }


  #[test]
  fn left_identity_u16_t1(x in any::<u16>()) {
    prop_assert!(left_identity(&(x,)))
  }


  #[test]
  fn left_identity_u16_a1(x in any::<u16>()) {
    prop_assert!(left_identity(&[x]))
  }


  #[test]
  fn left_identity_i64(x in any::<i64>()) {
    prop_assert!(left_identity(&x))
  }


  #[test]
  fn left_identity_i32_t2(xs in any::<(i32, i32)>()) {
    prop_assert!(left_identity(&xs))
  }


  #[test]
  fn left_identity_i32_a2(xs in any::<[i32; 2]>()) {
    prop_assert!(left_identity(&xs))
  }


  #[test]
  fn right_identity_u32(x in any::<u32>()) {
    prop_assert!(right_identity(&x))
  }


  #[test]
  fn right_identity_u8_t1(x in any::<u8>()) {
    prop_assert!(right_identity(&(x,)))
  }


  #[test]
  fn right_identity_u8_a1(x in any::<u8>()) {
    prop_assert!(right_identity(&[x]))
  }


  #[test]
  fn right_identity_isize(x in any::<isize>()) {
    prop_assert!(right_identity(&x))
  }


  #[test]
  fn right_identity_i32_t3(xs in any::<(i32, i32, i32)>()) {
    prop_assert!(right_identity(&xs))
  }


  #[test]
  fn right_identity_i32_a3(xs in any::<[i32; 3]>()) {
    prop_assert!(right_identity(&xs))
  }


  #[test]
  fn left_identity_f32(x in TF32) {
    prop_assert!(num_left_identity(&x, &F32_EPS))
  }


  #[test]
  fn right_identity_f64(xs in TF64) {
    prop_assert!(num_right_identity(&xs, &F64_EPS))
 }
}
