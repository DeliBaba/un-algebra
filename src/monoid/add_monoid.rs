//!
//! Algebraic _additive_ _monoids_.
//!
//! An algebraic _additive_ _monoid_ is an _additive_ _semigroup_ `S`,
//! with a unique additive _identity_ element, called _zero_, and
//! denoted `0`.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x ∈ S
//!
//! Identity: ∃0 ∈ S: 0 + x = x + 0 = x.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive monoid.
//!
#![doc(include = "../doc/references.md")]

use crate::numeric::*;
use crate::semigroup::*;


///
/// An algebraic _additive monoid_.
///
pub trait AddMonoid: AddSemigroup {

  /// The unique _additive_ _identity_ (i.e. zero) element. This should
  /// be an associated const, but we need more upstream crates to
  /// support them first.
  fn zero() -> Self;


  /// Test for the additive identity element.
  fn is_zero(&self) -> bool {
    *self == Self::zero()
  }
}


///
/// The left _additive_ _identity_ axiom.
///
pub fn left_identity<T: AddMonoid>(x: &T) -> bool {
  T::zero().add(x) == *x
}


///
/// The right _additive_ _identity_ axiom.
///
pub fn right_identity<T: AddMonoid>(x: &T) -> bool {
  x.add(&T::zero()) == *x
}


///
/// The two sided _additive_ _identity_ axiom.
///
pub fn identity<T: AddMonoid>(x: &T) -> bool {
  left_identity(x) && right_identity(x)
}


///
/// The _left_ _numeric_ _additive_ _identity_ axiom.
///
pub fn num_left_identity<T: AddMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  T::zero().add(x).num_eq(x, eps)
}


///
/// The _right_ _numeric_ _additive_ _identity_ axiom.
///
pub fn num_right_identity<T: AddMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  x.add(&T::zero()).num_eq(x, eps)
}


///
/// The _two_ _sided_ _numeric_ _additive_ identity axiom.
///
pub fn num_identity<T: AddMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  num_left_identity(x, eps) && num_right_identity(x, eps)
}


///
/// A macro for `AddMonoid` implementations for built-in numeric types.
///
macro_rules! numeric_add_monoid {
  ($type:ty) => {
    impl AddMonoid for $type {

      /// Zero is just type zero.
      fn zero() -> Self {
        0 as Self
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    numeric_add_monoid! {$type}
    numeric_add_monoid! {$($others),+}
  };
}


// Numeric additive monoids.
numeric_add_monoid! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form an additive monoid.
///
impl AddMonoid for () {

  /// Zero value can only be `()`.
  fn zero() -> Self {}
}


///
/// 1-tuples form an additive monoid when their items do.
///
impl<T: AddMonoid> AddMonoid for (T,) {

  /// Zero is by element type.
  fn zero() -> Self {
    (T::zero(), )
  }
}


///
/// 2-tuples form an additive monoid when their items do.
///
impl<A: AddMonoid, B: AddMonoid> AddMonoid for (A, B) {

  /// Zero is by element type.
  fn zero() -> Self {
    (A::zero(), B::zero())
  }
}


///
/// 3-tuples form an additive monoid when their items do.
///
impl<A: AddMonoid, B: AddMonoid, C: AddMonoid> AddMonoid for (A, B, C) {

  /// Zero is by element type.
  fn zero() -> Self {
    (A::zero(), B::zero(), C::zero())
  }
}


///
/// A macro for `AddMonoid` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_add_monoid {
  ($size:expr) => {
    impl<T: Copy + AddMonoid> AddMonoid for [T; $size] {

      // Create array of zeros.
      fn zero() -> Self {
        [T::zero(); $size]
      }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_add_monoid! {$size}
    array_add_monoid! {$($others),+}
  };
}


// Array additive monoid types.
array_add_monoid! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;

