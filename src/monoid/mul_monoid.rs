//!
//! Algebraic _multiplicative_ _monoids_.
//!
//! An algebraic _multiplicative_ _monoid_ is a _multiplicative_
//! _semigroup_ `S`, with a unique multiplicative _identity_ element,
//! called _one_, and denoted `1`.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x ∈ S
//!
//! Identity: ∃1 ∈ S: 1 × x = x × 1 = x.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a multiplicative
//! monoid.
//!
#![doc(include = "../doc/references.md")]

use crate::numeric::*;
use crate::semigroup::*;


///
/// An algebraic _multiplicative monoid_.
///
pub trait MulMonoid: MulSemigroup {

  /// The unique _multiplicative_ _identity_ (i.e. one) element. This
  /// should be an associated const, but we need more upstream crates to
  /// support them first.
  fn one() -> Self;


  /// Test for the one (multiplicative identity) element.
  fn is_one(&self) -> bool {
    *self == Self::one()
  }
}


///
/// The _left_ _multiplicative_ _identity_ axiom.
///
pub fn left_identity<T: MulMonoid>(x: &T) -> bool {
  T::one().mul(x) == *x
}


///
/// The _right_ _multiplicative_ _identity_ axiom.
///
pub fn right_identity<T: MulMonoid>(x: &T) -> bool {
  x.mul(&T::one()) == *x
}


///
/// The _two_ _sided_ _multiplicative_ identity axiom.
///
pub fn identity<T: MulMonoid>(x: &T) -> bool {
  left_identity(x) && right_identity(x)
}


///
/// The _left_ _numeric_ _multiplicative_ _identity_ axiom.
///
pub fn num_left_identity<T: MulMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  T::one().mul(x).num_eq(x, eps)
}


///
/// The _right_ _numeric_ _multiplicative_ _identity_ axiom.
///
pub fn num_right_identity<T: MulMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  x.mul(&T::one()).num_eq(x, eps)
}


///
/// The _two_ _sided_ _numeric_ _multiplicative_ identity axiom.
///
pub fn num_identity<T: MulMonoid + NumEq>(x: &T, eps: &T::Eps) -> bool {
  num_left_identity(x, eps) && num_right_identity(x, eps)
}


///
/// A macro for `MulMonoid` implementations for built-in numeric types.
///
macro_rules! numeric_mul_monoid {
  ($type:ty) => {
    impl MulMonoid for $type {

      /// One is just type one.
      fn one() -> Self {
        1 as Self
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    numeric_mul_monoid! {$type}
    numeric_mul_monoid! {$($others),+}
  };
}


// Numeric multiplicative monoids.
numeric_mul_monoid! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form a multiplicative monoid.
///
impl MulMonoid for () {

  /// One value can only be `()`.
  fn one() -> Self {}
}


///
/// 1-tuples form a multiplicative monoid when their items do.
///
impl<T: MulMonoid> MulMonoid for (T,) {

  /// One is by element type.
  fn one() -> Self {
    (T::one(), )
  }
}


///
/// 2-tuples form a multiplicative monoid when their items do.
///
impl<A: MulMonoid, B: MulMonoid> MulMonoid for (A, B) {

  /// One is by element type.
  fn one() -> Self {
    (A::one(), B::one())
  }
}


///
/// 3-tuples form a multiplicative monoid when their items do.
///
impl<A: MulMonoid, B: MulMonoid, C: MulMonoid> MulMonoid for (A, B, C) {

  /// One is by element type.
  fn one() -> Self {
    (A::one(), B::one(), C::one())
  }
}


///
/// A macro for `MulMonoid` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_mul_monoid {
  ($size:expr) => {
    impl<T: Copy + MulMonoid> MulMonoid for [T; $size] {

      // Create array of ones.
      fn one() -> Self {
        [T::one(); $size]
      }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_mul_monoid! {$size}
    array_mul_monoid! {$($others),+}
  };
}


// Array multiplicative monoid types.
array_mul_monoid! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;





