//!
//! Generative tests for _order_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn antisymmetry_le_f32((x, y) in (F32, F32)) {
    prop_assert!(partial_order::num_antisymmetry(&f32::num_le, &x, &y, &F32_EPS))
  }


  #[test]
  fn antisymmetry_ge_f32((x, y) in (F32, F32)) {
    prop_assert!(partial_order::num_antisymmetry(&f32::num_ge, &x, &y, &F32_EPS))
  }


  #[test]
  fn connexity_le_f64((x, y) in (F64, F64)) {
    prop_assert!(total_order::num_connexity(&f64::num_le, &x, &y, &F64_EPS))
  }


  #[test]
  fn connexity_ge_f64((x, y) in (F64, F64)) {
    prop_assert!(total_order::num_connexity(&f64::num_ge, &x, &y, &F64_EPS))
  }


  #[test]
  fn reflexivity_le_f32(x in F32) {
    prop_assert!(partial_order::num_reflexivity(&f32::num_le, &x, &F32_EPS))
  }


  #[test]
  fn reflexivity_ge_f32(x in F32) {
    prop_assert!(partial_order::num_reflexivity(&f32::num_ge, &x, &F32_EPS))
  }


  #[test]
  fn irreflexivity_le_f64(x in F64) {
    prop_assert!(strict_order::num_irreflexivity(&f64::num_lt, &x, &F64_EPS))
  }


  #[test]
  fn irreflexivity_ge_f64(x in F64) {
    prop_assert!(strict_order::num_irreflexivity(&f64::num_gt, &x, &F64_EPS))
  }


  #[test]
  fn asymmetry_le_f32((x, y) in (F32, F32)) {
    prop_assert!(strict_order::num_asymmetry(&f32::num_le, &x, &y, &F32_EPS))
  }


  #[test]
  fn asymmetry_ge_f32((x, y) in (F32, F32)) {
    prop_assert!(strict_order::num_asymmetry(&f32::num_ge, &x, &y, &F32_EPS))
  }


  #[test]
  fn transitivity_le_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(partial_order::num_transitivity(&f64::num_le, &x, &y, &z, &F64_EPS))
  }


  #[test]
  fn transitivity_ge_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(partial_order::num_transitivity(&f64::num_ge, &x, &y, &z, &F64_EPS))
  }


  #[test]
  fn transitivity_lt_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(strict_order::num_transitivity(&f64::num_lt, &x, &y, &z, &F64_EPS))
  }


  #[test]
  fn transitivity_gt_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(strict_order::num_transitivity(&f64::num_gt, &x, &y, &z, &F64_EPS))
  }
}
