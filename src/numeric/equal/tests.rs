//!
//! Generative tests for _equality_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn symmetry_f32((x, y) in (F32, F32)) {
    prop_assert!(equivalence::num_symmetry(&f32::num_eq, &x, &y, &F32_EPS))
  }


  #[test]
  fn symmetry_f64((x, y) in (F64, F64)) {
    prop_assert!(equivalence::num_symmetry(&f64::num_eq, &x, &y, &F64_EPS))
  }


  #[test]
  fn reflexivity_f32(x in F32) {
    prop_assert!(equivalence::num_reflexivity(&f32::num_eq, &x, &F32_EPS))
  }


  #[test]
  fn reflexivity_f64(x in F64) {
    prop_assert!(equivalence::num_reflexivity(&f64::num_eq, &x, &F64_EPS))
  }


  #[test]
  fn transitivity_f32((x, y, z) in (F32, F32, F32)) {
    // Larger error term to handle very large floats.
    prop_assert!(equivalence::num_transitivity(&f32::num_eq, &x, &y, &z, &1.0))
  }


  #[test]
  fn transitivity_f64((x, y, z) in (F64, F64, F64)) {
    // Larger error term to handle very large floats.
    prop_assert!(equivalence::num_transitivity(&f64::num_eq, &x, &y, &z, &1.0))
  }


  #[test]
  fn ne_symmetry_f32((x, y) in (F32, F32)) {
    prop_assert!(inequality::num_symmetry(&f32::num_ne, &x, &y, &F32_EPS))
  }


  #[test]
  fn ne_symmetry_f64((x, y) in (F64, F64)) {
    prop_assert!(inequality::num_symmetry(&f64::num_ne, &x, &y, &F64_EPS))
  }


  #[test]
  fn irreflexivity_f32(x in F32) {
    prop_assert!(inequality::num_irreflexivity(&f32::num_ne, &x, &F32_EPS))
  }


  #[test]
  fn irreflexivity_f64(x in F64) {
    prop_assert!(inequality::num_irreflexivity(&f64::num_ne, &x, &F64_EPS))
  }
}
