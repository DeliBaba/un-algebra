//!
//! Generative tests for _multiplicative_ _semigroups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn associativity_u16([x, y, z] in any::<[u16; 3]>()) {
    prop_assert!(associativity(&x, &y, &z))
  }


  #[test]
  fn associativity_u32_t1([x, y, z] in any::<[u32; 3]>()) {
    prop_assert!(associativity(&(x,), &(y,), &(z,)))
  }


  #[test]
  fn associativity_u64_a1([x, y, z] in any::<[u64; 3]>()) {
    prop_assert!(associativity(&[x], &[y], &[z]))
  }


  #[test]
  fn associativity_isize([x, y, z] in any::<[isize; 3]>()) {
    prop_assert!(associativity(&x, &y, &z))
  }


  #[test]
  fn associativity_i32_t2([xs, ys, zs] in any::<[(i32, i32); 3]>()) {
    prop_assert!(associativity(&xs, &ys, &zs))
  }


  #[test]
  fn associativity_a2([xs, ys, zs] in any::<[[i64; 2]; 3]>()) {
    prop_assert!(associativity(&xs, &ys, &zs))
  }


  #[test]
  fn associativity_f32([x, y, z] in [TF32, TF32, TF32]) {
    prop_assert!(num_associativity(&x, &y, &z, &F32_EPS))
  }


  #[test]
  fn associativity_f64([x, y, z] in [TF64, TF64, TF64]) {
    prop_assert!(num_associativity(&x, &y, &z, &F64_EPS))
  }
}
