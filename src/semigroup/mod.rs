//!
//! Algebraic _semigroup_ modules.
//!
//! The `semigroup` module provides sub-modules for abstract algebraic
//! _semigroups_. It also provides sub-modules for _additive_ or
//! _multiplicative semigroups_.
//!
pub mod semigroup;
pub mod add_semigroup;
pub mod mul_semigroup;


// Make sub-modules visible in this module.
pub use self::semigroup::*;
pub use self::add_semigroup::*;
pub use self::mul_semigroup::*;
