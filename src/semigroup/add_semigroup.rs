//!
//! Algebraic _additive_ _semigroups_.
//!
//! An algebraic _additive_ _semigroup_ is an _additive_ _magma_ `M`,
//! where the `addition` operation `+` is _associative_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x, y, z ∈ M
//!
//! Associativity: (x + y) + z = x + (y + z).
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive
//! semigroup.
//!
#![doc(include = "../doc/references.md")]

use crate::magma::*;
use crate::numeric::*;


///
/// An algebraic _additive semigroup_.
///
pub trait AddSemigroup: AddMagma {}



///
/// The _associativity_ axiom.
///
pub fn associativity<T: AddSemigroup>(x: &T, y: &T, z: &T) -> bool {
  x.add(&y.add(z)) == x.add(y).add(z)
}


//
/// The _numeric_ _associativity_ axiom.
///
pub fn num_associativity<T: AddSemigroup + NumEq>(x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  x.add(&y.add(z)).num_eq(&x.add(y).add(z), eps)
}


///
/// A macro for `AddSemigroup` implementations for built-in numeric
/// types.
///
macro_rules! numeric_add_semigroup {
  ($type:ty) => {
    impl AddSemigroup for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_add_semigroup! {$type}
    numeric_add_semigroup! {$($others),+}
  };
}


// Numeric additive semigroups.
numeric_add_semigroup! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form an additive semigroup.
///
impl AddSemigroup for () {}


///
/// 1-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup> AddSemigroup for (A,) {}


///
/// 2-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup, B: AddSemigroup> AddSemigroup for (A, B) {}


///
/// 3-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup, B: AddSemigroup, C: AddSemigroup> AddSemigroup for (A, B, C) {}


///
/// A macro for `AddSemigroup` implementations for arrays. Maybe not
/// needed if Rust had _const_ _generics_.
///
macro_rules! array_add_semigroup {
  ($size:expr) => {
    impl<T: Copy + AddSemigroup> AddSemigroup for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_add_semigroup! {$size}
    array_add_semigroup! {$($others),+}
  };
}


// Array additive semigroup types.
array_add_semigroup! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;



