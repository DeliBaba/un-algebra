//!
//! Algebraic _multiplicative_ _semigroups_.
//!
//! An algebraic _multiplicative_ _semigroup_ is a _multiplicative_
//! _magma_ `M`, where the multiplication operation `×` is
//! _associative_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x, y, z ∈ M
//!
//! Associativity: (x × y) × z = x × (y × z).
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a multiplicative
//! semigroup.
//!
#![doc(include = "../doc/references.md")]

use crate::magma::*;
use crate::numeric::*;


///
/// An algebraic _multiplicative semigroup_.
///
pub trait MulSemigroup: MulMagma {}


///
/// The _associativity_ axiom.
//
pub fn associativity<T: MulSemigroup>(x: &T, y: &T, z: &T) -> bool {
  x.mul(&y.mul(z)) == x.mul(y).mul(z)
}


///
/// The _numeric_ _associativity_ axiom.
///
pub fn num_associativity<T: MulSemigroup + NumEq>(x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  x.mul(&y.mul(z)).num_eq(&x.mul(y).mul(z), eps)
}


///
/// A macro for `MulSemigroup` implementations for built-in numeric
/// types.
///
macro_rules! numeric_mul_semigroup {
  ($type:ty) => {
    impl MulSemigroup for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_mul_semigroup! {$type}
    numeric_mul_semigroup! {$($others),+}
  };
}


// Numeric multiplicative semigroups.
numeric_mul_semigroup! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form a multiplicative semigroup.
///
impl MulSemigroup for () {}


///
/// 1-tuples form a multiplicative semigroup when their items do.
///
impl<A: MulSemigroup> MulSemigroup for (A,) {}


///
/// 2-tuples form a multiplicative semigroup when their items do.
///
impl<A: MulSemigroup, B: MulSemigroup> MulSemigroup for (A, B) {}


///
/// 3-tuples form a multiplicative semigroup when their items do.
///
impl<A: MulSemigroup, B: MulSemigroup, C: MulSemigroup> MulSemigroup for (A, B, C) {}


///
/// A macro for `MulSemigroup` implementations for arrays. Maybe not
/// needed if Rust had _const_ _generics_.
///
macro_rules! array_mul_semigroup {
  ($size:expr) => {
    impl<T: Copy + MulSemigroup> MulSemigroup for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_mul_semigroup! {$size}
    array_mul_semigroup! {$($others),+}
  };
}


// Array multiplicative semigroup types.
array_mul_semigroup! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;



