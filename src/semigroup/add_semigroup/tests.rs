//!
//! Generative tests for _additive_ _semigroups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn associativity_u32([x, y, z] in any::<[u32; 3]>()) {
    prop_assert!(associativity(&x, &y, &z))
  }


  #[test]
  fn associativity_u64_t2([x, y, z] in any::<[u64; 3]>()) {
    prop_assert!(associativity(&(x,), &(y,), &(z,)))
  }


  #[test]
  fn associativity_u64_a1([x, y, z] in any::<[u64; 3]>()) {
    prop_assert!(associativity(&[x], &[y], &[z]))
  }


  #[test]
  fn associativity_i8([x, y, z] in any::<[i8; 3]>()) {
    prop_assert!(associativity(&x, &y, &z))
  }


  #[test]
  fn associativity_t2([xs, ys, zs] in any::<[(isize, isize); 3]>()) {
    prop_assert!(associativity(&xs, &ys, &zs))
  }


  #[test]
  fn associativity_a2([xs, ys, zs] in any::<[[i64; 2]; 3]>()) {
    prop_assert!(associativity(&xs, &ys, &zs))
  }


  #[test]
  fn associativity_f32([x, y, z] in [TF32, TF32, TF32]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_associativity(&x, &y, &z, &1.0))
  }


  #[test]
  fn associativity_f64([x, y, z] in [TF64, TF64, TF64]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_associativity(&x, &y, &z, &(F64_EPS * 1e4)))
  }
}
