//!
//! Algebraic _ring_ traits.
//!
//! An algebraic _ring_ `R`, is an _additive_ _commutative_ _group_
//! **and** a _multiplicative_ _monoid_, and therefore has both
//! _addition_ `+` and _multiplication_ `×` operators. In addition to
//! group and monoid axioms ring multiplication is required to
//! _distribute_ over addition.
//!
//! Because of their additive group aspect, rings have a unique `0`
//! additive identity element. Not all authors require rings to have a
//! `1` multiplicative identity element, but in `un_algebra` they do,
//! i.e. `un_algebra` rings are _rings_ _with_ _unity_.
//!
//! # Axioms
//!
//! ```notrust
//! ∀x, y, z ∈ R
//!
//! Distributivity (left): x × (y + z) = (x × y) + (x × z).
//! Distributivity (right): (x + y) × z = (x × z) + (y × z).
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a ring.
//!
#![doc(include = "../doc/references.md")]

use crate::monoid::*;
use crate::helpers::*;
use crate::numeric::*;
use crate::com_group::*;


///
/// An algebraic _ring_.
///
pub trait Ring: AddComGroup + MulMonoid {}


///
/// The axiom of _left_ _distributivity_ (multiplication).
///
pub fn left_distributivity<T: Ring>(x: &T, y: &T, z: &T) -> bool {
  x.mul(&y.add(z)) == x.mul(y).add(&x.mul(z))
}


///
/// The axiom of _right_ _distributivity_ (multiplication).
///
pub fn right_distributivity<T: Ring>(x: &T, y: &T, z: &T) -> bool {
  y.add(z).mul(x) == y.mul(x).add(&z.mul(x))
}


///
/// The axiom of _two_ _sided_ _distributivity_ (multiplication).
///
pub fn distributivity<T: Ring>(x: &T, y: &T, z: &T) -> bool {
  left_distributivity(x, y, z) && right_distributivity(x, y, z)
}


///
/// The derived property of _left_ _absorption_ (zero).
///
pub fn left_absorption<T: Ring>(x: &T) -> bool {
  x.mul(&T::zero()) == T::zero()
}


///
/// The derived property of _right_ _absorption_ (zero).
///
pub fn right_absorption<T: Ring>(x: &T) -> bool {
  T::zero().mul(x) == T::zero()
}


///
/// The derived property of _two_ _sided_ _absorption_ (zero).
///
pub fn absorption<T: Ring>(x: &T) -> bool {
  left_absorption(x) && right_absorption(x)
}


///
/// The derived property of _left_ _negation_ (multiplicative).
///
pub fn left_negation<T: Ring>(x: &T, y: &T) -> bool {
  x.negate().mul(y) == x.mul(y).negate()
}


///
/// The derived property of _right_ _negation_ (multiplicative).
///
pub fn right_negation<T: Ring>(x: &T, y: &T) -> bool {
  x.mul(&y.negate()) == x.mul(y).negate()
}


///
/// The derived property of _two_ _sided_ _negation_ (multiplicative).
///
pub fn negation<T: Ring>(x: &T, y: &T) -> bool {
  left_negation(x, y) && right_negation(x, y)
}


///
/// The derived property of _left_ _zero_ _divisors_.
///
pub fn left_zero_divisors<T: Ring>(x: &T, y: &T) -> bool {
  imply(x.mul(y).is_zero(), x.is_zero() || y.is_zero())
}


///
/// The derived property of _right_ _zero_ _divisors_.
///
pub fn right_zero_divisors<T: Ring>(x: &T, y: &T) -> bool {
  imply(y.mul(x).is_zero(), x.is_zero() || y.is_zero())
}


///
/// The derived property of _two_ _sided_ _zero_ _divisors_.
///
pub fn zero_divisors<T: Ring>(x: &T, y: &T) -> bool {
  left_zero_divisors(x, y) && right_zero_divisors(x, y)
}


///
/// The axiom of _numeric_ _left_ _distributivity_ (multiplication).
///
pub fn num_left_distributivity<T: Ring + NumEq>(x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  x.mul(&y.add(z)).num_eq(&x.mul(y).add(&x.mul(z)), eps)
}


///
/// The axiom of _numeric_ _right_ _distributivity_ (multiplication).
///
pub fn num_right_distributivity<T: Ring + NumEq>(x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  y.add(z).mul(x).num_eq(&y.mul(x).add(&z.mul(x)), eps)
}


///
/// The axiom of _numeric_ _two_ _sided_ _distributivity_
/// (multiplication).
///
pub fn num_distributivity<T: Ring + NumEq>(x: &T, y: &T, z: &T, eps: &T::Eps) -> bool {
  num_left_distributivity(x, y, z, eps) && num_right_distributivity(x, y, z, eps)
}


///
/// The derived property of _numeric_ _left_ _absorption_ (zero).
///
pub fn num_left_absorption<T: Ring + NumEq>(x: &T, eps: &T::Eps) -> bool {
  x.mul(&T::zero()).num_eq(&T::zero(), eps)
}


///
/// The derived property of _numeric_ _right_ _absorption_ (zero).
///
pub fn num_right_absorption<T: Ring + NumEq>(x: &T, eps: &T::Eps) -> bool {
  T::zero().mul(x).num_eq(&T::zero(), eps)
}


///
/// The derived property of _numeric_ _two_ _sided_ _absorption_ (zero).
///
pub fn num_absorption<T: Ring + NumEq>(x: &T, eps: &T::Eps) -> bool {
  num_left_absorption(x, eps) && num_right_absorption(x, eps)
}


///
/// The derived property of _numeric_ _left_ _negation_
/// (multiplicative).
///
pub fn num_left_negation<T: Ring + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  x.negate().mul(y).num_eq(&x.mul(y).negate(), eps)
}


///
/// The derived property of _numeric_ _right_ _negation_
/// (multiplicative).
///
pub fn num_right_negation<T: Ring + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  x.mul(&y.negate()).num_eq(&x.mul(y).negate(), eps)
}


///
/// The derived property of _numeric_ _two_ _sided_ _negation_
/// (multiplicative).
///
pub fn num_negation<T: Ring + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  num_left_negation(x, y, eps) && num_right_negation(x, y, eps)
}


///
/// The derived property of _numeric_ _left_ _zero_ _divisors_.
///
pub fn num_left_zero_divisors<T: Ring + NumEq>(x: &T, y: &T, _eps: &T::Eps) -> bool {
  imply(x.mul(y).is_zero(), x.is_zero() || y.is_zero())
}


///
/// The derived property of _numeric_ _right_ _zero_ _divisors_.
///
pub fn num_right_zero_divisors<T: Ring + NumEq>(x: &T, y: &T, _eps: &T::Eps) -> bool {
  imply(y.mul(x).is_zero(), x.is_zero() || y.is_zero())
}


///
/// The derived property of _numeric_ _two_ _sided_ _zero_ _divisors_.
///
pub fn num_zero_divisors<T: Ring + NumEq>(x: &T, y: &T, eps: &T::Eps) -> bool {
  num_left_zero_divisors(x, y, eps) && num_right_zero_divisors(x, y, eps)
}


///
/// A macro for `Ring` implementations for built-in numeric types.
///
macro_rules! numeric_ring {
  ($type:ty) => {
    impl Ring for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_ring! {$type}
    numeric_ring! {$($others),+}
  };
}


// Only signed integers and floats form a ring.
numeric_ring! {
  i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form a ring.
///
impl Ring for () {}


///
/// 1-tuples form a ring when their items do.
///
impl<A: Ring> Ring for (A,) {}


///
/// 2-tuples form a ring when their items do.
///
impl<A: Ring, B: Ring> Ring for (A, B) {}


///
/// 3-tuples form a ring when their items do.
///
impl<A: Ring, B: Ring, C: Ring> Ring for (A, B, C) {}


///
/// A macro for `Ring` implementations for arrays. Maybe not needed if
/// Rust had _const_ _generics_.
///
macro_rules! array_ring {
  ($size:expr) => {
    impl<T: Copy + Ring> Ring for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_ring! {$size}
    array_ring! {$($others),+}
  };
}


// Array ring types.
array_ring! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
