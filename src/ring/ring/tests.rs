//!
//! Generative tests for _rings_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn left_distributivity_i32([x, y, z] in any::<[i32; 3]>()) {
    prop_assert!(left_distributivity(&x, &y, &z))
  }


  #[test]
  fn left_distributivity_i8_t1([x, y, z] in any::<[i8; 3]>()) {
    prop_assert!(left_distributivity(&(x,), &(y,), &(z,)))
  }


  #[test]
  fn left_distributivity_i8_a1([x, y, z] in any::<[i8; 3]>()) {
    prop_assert!(left_distributivity(&[x], &[y], &[z]))
  }


  #[test]
  fn right_distributivity_i16([x, y, z] in any::<[i16; 3]>()) {
    prop_assert!(right_distributivity(&x, &y, &z))
  }


  #[test]
  fn right_distributivity_i64_t2([xs, ys, zs] in any::<[(i64, i64); 3]>()) {
    prop_assert!(right_distributivity(&xs, &ys, &zs))
  }


  #[test]
  fn right_distributivity_i64_a2([xs, ys, zs] in any::<[[i64; 2]; 3]>()) {
    prop_assert!(right_distributivity(&xs, &ys, &zs))
  }


  #[test]
  fn left_absorption_isize(x in any::<isize>()) {
    prop_assert!(left_absorption(&x))
  }


  #[test]
  fn right_absorption_i32(x in any::<i32>()) {
    prop_assert!(right_absorption(&x))
  }


  #[test]
  fn right_absorption_i16_t2(xs in any::<(i16, i16)>()) {
    prop_assert!(right_absorption(&xs))
  }


  #[test]
  fn right_absorption_i16_a2(xs in any::<[i16; 2]>()) {
    prop_assert!(right_absorption(&xs))
  }


  #[test]
  fn left_negation_i16((x, y) in any::<(i16, i16)>()) {
    prop_assert!(left_negation(&x, &y))
  }


  #[test]
  fn left_negation_i32_t2([xs, ys] in any::<[(i32, i32); 2]>()) {
    prop_assert!(left_negation(&xs, &ys))
  }


  #[test]
  fn left_negation_i32_a2([xs, ys] in any::<[[i32; 2]; 2]>()) {
    prop_assert!(left_negation(&xs, &ys))
  }


  #[test]
  fn right_negation_i32((x, y) in any::<(i32, i32)>()) {
    prop_assert!(right_negation(&x, &y))
  }


  #[test]
  fn right_negation_i8_t3([xs, ys] in any::<[(i8, i8, i8); 2]>()) {
    prop_assert!(right_negation(&xs, &ys))
  }


  #[test]
  fn right_negation_i8_a3([xs, ys] in any::<[[i8; 3]; 2]>()) {
    prop_assert!(right_negation(&xs, &ys))
  }


  #[test]
  fn left_distributivity_f32([x, y, z] in [TF32, TF32, TF32]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_left_distributivity(&x, &y, &z, &1.0))
  }


  #[test]
  fn left_distributivity_f64([x, y, z] in [TF64, TF64, TF64]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_left_distributivity(&x, &y, &z, &0.1))
  }


  #[test]
  fn right_distributivity_f32([x, y, z] in [TF32, TF32, TF32]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_right_distributivity(&x, &y, &z, &1.0))
  }


  #[test]
  fn right_distributivity_f64([x, y, z] in [TF64, TF64, TF64]) {
    // Larger error term due to cancellation issues.
    prop_assert!(num_right_distributivity(&x, &y, &z, &0.1))
  }


  #[test]
  fn left_absorption_f32(x in TF32) {
    prop_assert!(num_left_absorption(&x, &F32_EPS))
  }


  #[test]
  fn left_absorption_f64_t3(xs in [TF64, TF64, TF64]) {
    prop_assert!(num_left_absorption(&xs, &F64_EPS))
  }


  #[test]
  fn left_absorption_f64_a3(xs in [TF64, TF64, TF64]) {
    prop_assert!(num_left_absorption(&xs, &F64_EPS))
  }


  #[test]
  fn right_absorption_f64(x in TF64) {
    prop_assert!(num_right_absorption(&x, &F64_EPS))
  }


  #[test]
  fn left_negation_f32([x, y] in [TF32, TF32]) {
    prop_assert!(num_left_negation(&x, &y, &F32_EPS))
  }


  #[test]
  fn left_negation_f64_t1([x, y] in [TF64, TF64]) {
    prop_assert!(num_left_negation(&(x,), &(y,), &F64_EPS))
  }


  #[test]
  fn left_negation_f64_a1([x, y] in [TF64, TF64]) {
    prop_assert!(num_left_negation(&[x], &[y], &F64_EPS))
  }


  #[test]
  fn right_negation_f64([x, y] in [TF64, TF64]) {
    prop_assert!(num_right_negation(&x, &y, &F64_EPS))
  }
}
