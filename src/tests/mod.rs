//!
//! Generative testing support for `un_algebra` axioms and
//! properties.
//!
//! The `tests` module provides random value generators and testing
//! configuration helper functions for `un_algebra` generative tests.
//!
#![doc(include = "../doc/references.md")]

pub mod float;
pub mod config;


// Make sub-modules visible in this module.
pub use self::config::*;
pub use self::float::*;


// Make `proptest` modules visible in this module.
pub use proptest::*;
pub use proptest::prelude::*;
pub use proptest::test_runner::*;





