//!
//! Floating point error bounds for generative tests.
//!
//! `un_algebra` generative tests (using `proptest`) rely on randomly
//! generated values of floating point values. The `float` module
//! provides floating point test value and error bounds.
//!
pub use std::ops::*;


/// Default testing epsilon for 32 bit FP comparisons.
pub const F32_EPS: f32 = 1e-6;


/// Default testing epsilon for 64 bit FP comparisons.
pub const F64_EPS: f64 = 1e-14;


/// Test range for general 32 bit float tests.
pub const F32: std::ops::Range<f32> = -1e35..1e35;


/// Test range for general 64 bit float tests.
pub const F64: std::ops::Range<f64> = -1e305..1e305;


/// Test range for overflow prone 32 bit float tests.
pub const TF32: std::ops::Range<f32> = -1.0e12..1.0e12;


/// Test range for overflow prone 64 bit float tests.
pub const TF64: std::ops::Range<f64> = -1.0e100..1.0e100;

