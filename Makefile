# A simple GNU Makefile for performing Rust (i.e. Cargo) commands at
# the command line. Using a Makefile this way may not be idiomatic
# Rust development practice.


# Assume cargo is installed in our home directory.
CARGO :=~/.cargo/bin/cargo


# Cargo parallel jobs limit.
JOBS := 32


# Cargo test threads limit.
THREADS := 32


# Use cargo check to check crate syntax.
check:
	${CARGO} check --jobs ${JOBS} --lib --examples --tests


# Use cargo build to build crate source and examples.
build:
	${CARGO} build --jobs ${JOBS} --examples


# Use cargo doc to create and view crate documentation.
docs:
	${CARGO} clean --doc
	${CARGO} doc --no-deps --open


# Use cargo test to run crate and example tests.
tests:
	${CARGO} test --jobs ${JOBS} --tests -- --test-threads=${THREADS}


# Use cargo test to run (release) crate and example tests.
tests-release:
	${CARGO} test --jobs ${JOBS} --tests --release -- --test-threads=${THREADS}


# Use cargo test to run crate example tests only.
examples:
	${CARGO} test --jobs ${JOBS} --examples -- --test-threads=16


# Use cargo test to run (release) crate example tests only.
examples-release:
	${CARGO} test --jobs ${JOBS} --examples --release -- --test-threads=${THREADS}


# Use clippy to do extra-pedantic linting of crate source.
lint:
	${CARGO} clean -p un_algebra
	${CARGO} clippy --all-targets -- \
	  -W clippy::pedantic \
	  -W clippy::nursery \
	  -W clippy::style \
	  -W clippy::cargo \
	  -W clippy::correctness \
	  -A clippy::module_inception \
    -A clippy::module_name_repetitions


# Summarize clippy lints by kind to the command line.
lint-summary:
	make lint 2>&1 | grep '^warning' | sort | uniq


.PHONY: lint
.PHONY: docs
.PHONY: check
.PHONY: build
.PHONY: tests
.PHONY: tests-release
.PHONY: examples
.PHONY: examples-release
.PHONY: lint-summary
