# Change Log

Notable changes to the `un_algebra` crate.


### [0.8.0] - 2019-07-29

### Added
Add `PartialOrder` relation trait.
Add `TotalOrder` relation trait.
Add `StrictOrder` relation trait.
Add `NumOrd` numeric ordering trait.
Add simple `Sequence` collection trait.
Add 4-tuple support to numeric comparisons.

### Changed
Change helper trait implementers to use `Sequence` trait.
Change example test value functions to return `impl Strategy`

### Removed
Remove `Map<T>`, `Any<T>` et. al. helper traits`.


### [0.7.0] - 2019-05-16

### Changed
- Minor documentation edits for clarity and layout.
- Combine numeric axioms and properties into non-numeric modules.

### Removed
- Deprecate numeric modules in favour of numeric axioms.


### [0.6.0] - 2019-05-03

### Changed
- Make minor code changes as per Clippy recommendations.
- Extract "numeric" structures into separate sub-modules.
- Move axioms and properties to separate function.

### Removed
- Remove final To-Do item in favour of Gitlab issues.


### [0.5.0] - 2019-04-26

### Added
- Add `relation` module with equivalence and inequality relations.
- Implement relevant relations for floating point types.
- Add relation property tests for NumEq implementers.
- Add new floating point test ranges.
- Add README stability notice.

### Changed
- Change numeric equality to use absolute and relative error terms.
- Use `proptest-derive` macros in example generative tests.
- Require V0.9.x series of `proptest` crate.
- Use standard test configuration for example tests.
- Update references and documentation.

### Removed
- Remove "default epsilon" numeric comparison methods.


### [0.4.0] - 2019-01-30

### Changed
- Add epsilon parameter to numeric `is_xyz()` methods.
- Consistently use `&self` parameter in axioms and properties.
- Add missing `prop_assert!` calls to generative tests.
- Fix test errors arising from previously false positive tests.
- Fix example errors arising from previously false positive tests.
- Rename quasigroup methods to use widely accepted terminology.
- Restrict floating point test values to avoid overflows.
- Defer some generative tests pending relative error comparisons.
- Refactor complex axiom boolean expressions for clarity.


### [0.3.0] - 2019-01-18

### Changed
- Rename example crates filenames to snake_case
- Rename test function suffixes to snake_case
- Rename numeric Error associated type to Eps
- Change numeric equality tests to better use helper methods
- Revert helper methods to closure type parameters
- Refactor helpers module methods
- Simplify additive magma closure calls

### Removed
- Remove most README To-Do's in favour of repository issues


### [0.2.2] - 2019-01-03

### Changed
- Minor README corrections.
- Minor F4 example corrections.
- Remove crate `types` module.
- Clarify array `impl` documentation.
- Moved rational number items to examples.
- Moved complex number items to examples.
- Rationalize helper traits in structures.


### [0.2.1] - 2018-12-23

### Changed
- Clarify module use documentation.
- Added array trait implementations for all structures.
- Simplified structure axiom function types.
- Re-organized structure source files for cohesion.
- Simplified helper module closure parameter types.

### Added
- Created new top-level helpers module.


### [0.2.0] - 2018-12-14

### Changed
- Changed to 2018 edition uniform paths.
- Moved unit tests to separate modules.
- Fixed quasigroup unit test failures.


### [0.1.7] - 2018-12-10

### Changed
- Added array trait implementations for numeric equality.
- Added array trait implementations for magmas.
- Reorganized magma source item order.
- Moved to individual magma test parameters.
- Clarify trait documentation text.

### Added
- Add Zip trait for "zipping" arrays and homogeneous tuples.


### [0.1.6] - 2018-11-26

### Changed
- Example tests make better use of proptest generator functions.
- Example structs now implement proptest Arbitrary trait.
- Library tests make better use of proptest generator functions.
- Update README.md on testing issues.


### [0.1.5] - 2018-11-19

### Changed
- Crate prelude now exports test helper modules.
- Reference new quasigroup traits in documentation.
- Fix minor whitespace and spelling issues.
- Bump version of proptest crate required.
- Rationalize use of tuples in axioms and test functions.

### Added
- Add quasigroup traits and example (<Z3,->).


### [0.1.4] - 2018-08-28

### Added
- Add release oriented Makefile targets.
- Implement semigroup traits for 0/1/2/3-tuples of values.
- Implement monoid traits for 0/1/2/3-tuples of values.
- Implement group traits for 0/1/2/3-tuples of values.
- Implement commutative group traits for 0/1/2/3-tuples of values.
- Implement ring traits for 0/1/2/3-tuples of values.
- Implement commutative ring traits for 0/1/2/3-tuples of values.
- Implement field traits for 0/1/2/3-tuples of values.
- Add 1/2/3-tuple generative tests to F2 example.


### [0.1.3] - 2018-08-20

### Added
- Implement numeric comparisons for 1/2/3-tuples of values.
- Implement magma traits for 0/1/2/3-tuples of values.
- Implement rock, paper, scissors example for tuples.


### [0.1.2] - 2018-08-15

### Changed
- Define macros for floating point trait implementations.
- Minor edits to in-module documentation.

### Added
- New Makefile targets.


### [0.1.1] - 2018-07-26

### Changed
- Minor corrections to in-module documentation.
- Clarify role of identity in rings and fields.

### Added
- Add new references to doc/reading.md


## [0.1.0] 2018-07-17

### Added
- Initial commit.
