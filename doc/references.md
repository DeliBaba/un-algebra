<!-- Markdown links for importing -->


<!-- Local modules and traits etc  -->

[`group`]: group::group

[`add_group`]: group::add_group

[`num_add_group`]: group::num_add_group

[`mul_group`]: group::mul_group

[`num_mul_group`]: group::num_mul_group

[`group::Group`]: group::group::Group

[`add_group::AddGroup`]: group::add_group::AddGroup

[`mul_group::MulGroup`]: group::mul_group::MulGroup

[`num_add_group::NumAddGroup`]: group::num_add_group::NumAddGroup

[`num_mul_group::NumMulgroup`]: group::num_mul_group::NumMulGroup



<!-- Local (repository) links -->

[references]: https://gitlab.com/ornamentist/un-algebra/blob/master/doc/reading.md

[issues]: https://gitlab.com/ornamentist/un-algebra/issues



<!-- External websites  -->

[IEEE]: https://floating-point-gui.de/



<!-- Other Rust crates  -->

[num]: https://crates.io/crates/num

[alga]: https://crates.io/crates/alga

[proptest]: https://crates.io/crates/proptest
